# DEPENDENCIES
sudo apt install libgl1-mesa-dev
sudo apt install libglew-dev
sudo apt install cmake

# OPTIONALS
sudo apt install ffmpeg libavcodec-dev libavutil-dev libavformat-dev libswscale-dev libavdevice-dev
sudo apt install libjpeg-dev libpng12-dev libtiff5-dev libopenexr-dev


# INSTALLATION
git clone https://github.com/stevenlovegrove/Pangolin.git

cd Pangolin
mkdir build
cd build
cmake ..
cmake --build .