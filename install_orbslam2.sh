C_DIR=$(pwd)

# DEPENDENCIES
source ./install_pangolin.sh
cd $C_DIR

source ./install_eigen.sh
cd $C_DIR

git config --global user.email "mihaescuac@gmail.com"
git config --global user.name "Alex Mihaescu"

git clone https://github.com/raulmur/ORB_SLAM2.git ORB_SLAM2

cd ./ORB_SLAM2
wget https://github.com/raulmur/ORB_SLAM2/pull/790.patch
git am 790.patch

cd ./Thirdparty/DBoW2
mkdir build && cd build
cmake ..
make -j12
sudo make install -j12

cd ../../g2o
mkdir build && cd build
cmake ..
make -j12
sudo make install -j12

cd "$C_DIR/ORB_SLAM2"
chmod +x build.sh
./build.sh

cd $C_DIR