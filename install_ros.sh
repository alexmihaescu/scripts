sudo apt-get install python-rosdep python-rosinstall-generator python-wstool python-rosinstall build-essential

sudo rosdep init
rosdep update

mkdir ~/ros_catkin_ws
cd ~/ros_catkin_ws

rosinstall_generator desktop --rosdistro melodic --deps --tar > melodic-desktop.rosinstall
wstool init -j8 src melodic-desktop.rosinstall

rosinstall_generator robot --rosdistro melodic --deps --tar > melodic-robot.rosinstall
wstool init -j8 src melodic-robot.rosinstall

rosdep install --from-paths src --ignore-src --rosdistro melodic -y